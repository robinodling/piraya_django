from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^members/', include('apps.members.urls')),
    url(r'^drinks/', include('apps.drinks.urls')),
    url(r'^os/', include('apps.otympligaspelen.urls')),
    
    # Keep this last
    url(r'^', include('apps.core.urls')),
)

handler404 = 'apps.core.views.not_found'
