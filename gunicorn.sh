#!/bin/bash

NAME="piraya" #Name of the application
DJANGODIR=/home/ubuntu/piraya # Django project directory
SOCKFILE=/home/ubuntu/piraya/piraya.sock # we will communicte using this unix socket

USER=ubuntu # the user to run as
GROUP=ubuntu # the group to run as
NUM_WORKERS=9 # how many worker processes should Gunicorn spawn

MAX_REQUESTS=1 # reload the application server for each request
DJANGO_SETTINGS_MODULE=piraya.settings # which settings file should Django use
DJANGO_WSGI_MODULE=piraya.wsgi # WSGI module name

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source ~/.virtualenvs/piraya/bin/activate

export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn

# Programs meant to be run under supervisor should not daemonize themselves (do not use -daemon)
exec ~/.virtualenvs/piraya/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
--name $NAME \
--workers $NUM_WORKERS \
--max-requests $MAX_REQUESTS \
--user=$USER --group=$GROUP \
--bind=0.0.0.0:8001 \
--log-level=error \
--log-file=-
