# -*- coding: utf-8 -*-
from django.core.mail import send_mail
from apps.otympligaspelen.models import Os, Team, Member

def register_check(POST):
    # Check team data
    if not ('team_name' in POST and len(POST['team_name']) > 0):
        return False
    if not ('team_email' in POST and len(POST['team_email']) > 0):
        return False
    if not ('team_contact' in POST and len(POST['team_contact']) > 0):
        return False
    if not ('team_phone' in POST and len(POST['team_phone']) > 0):
        return False
    if not ('team_motto' in POST and len(POST['team_motto']) > 0):
        return False
    
    # Check members data
    try:
        num_members = int(POST['num_members'])
    except:
        return False
    if num_members < 1 or num_members > 10:
        return False
    for i in xrange(num_members):
        member = "member_%d_" % i
        if not (member + 'name' in POST and len(POST[member + 'name']) > 0):
            return False
        if not (member + 'alias' in POST and len(POST[member + 'alias']) > 0):
            return False
        if not (member + 'function' in POST and len(POST[member + 'function']) > 0):
            return False
        if member + 'allergies' not in POST:
            return False
        if not (member + 'drink' in POST and POST[member + 'drink'] in ['O', 'C', 'L']):
            return False
        if not (member + 'sex' in POST and POST[member + 'sex'] in ['M', 'F']):
            return False
    
    return True

def register_add(os, POST):
    # Data already checked, just add
    team = Team(name=POST['team_name'], email=POST['team_email'],
                contact=POST['team_contact'], phone=POST['team_phone'],
                motto=POST['team_motto'], os=os)
    team.save()
    num_members = int(POST['num_members'])
    members = list()
    for i in xrange(num_members):
        member = "member_%d_" % i
        member = Member(name=POST[member + 'name'], alias=POST[member + 'alias'],
                        function=POST[member + 'function'], allergies=POST[member + 'allergies'],
                        drink=POST[member + 'drink'], sex=POST[member + 'sex'],
                        team=team)
        member.save()
        members.append(member)
    
    # Send email
    subject = u"[OS %d] Nytt lag registrerat" % os.year
    
    message = u"Namn: %s\nEmail: %s\nKontaktperson: %s\nTelefonnummer: %s\nMotto:\n%s\n\n" % (team.name, team.email, team.contact, team.phone, team.motto)
    
    if 'piraya_message' in POST and len(POST['piraya_message']) > 0:
        message += u"Meddelande till Piraya:\n%s\n\n" % POST['piraya_message']
    
    message += "Medlemmar:\n\n"
    for member in members:
        message += u"Namn: %s\nAlias: %s\nFunktion: %s\nAllergier: %s\nAlkoholpreferens: %s\nKön: %s\n\n" % (member.name, member.alias, member.function, member.allergies, member.get_drink_display(), member.get_sex_display())
    
    send_mail(subject, message, 'piraya@acc.umu.se', ['piraya@acc.umu.se'], fail_silently=True)
