from django.contrib import admin
from apps.otympligaspelen.models import Os, Team, Member

admin.site.register(Os)
admin.site.register(Team)

class MemberAdmin(admin.ModelAdmin):
    list_display = ('name', 'alias', 'function', 'team', 'allergies', 'drink', 'paid')

admin.site.register(Member, MemberAdmin)