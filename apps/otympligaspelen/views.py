# -*- coding: utf-8 -*-

import json
from django.shortcuts import render
from django.http import HttpResponse, Http404
from apps.otympligaspelen.models import Os, Team, Member
from apps.otympligaspelen.forms import register_check, register_add

def index(request):
    os_years = Os.objects.all().order_by('-year')
    return render(request, 'otympligaspelen/index.html', {
        'os_years': os_years,
    })

def os(request, year):
    try:
        os = Os.objects.get(year=year)
    except Os.DoesNotExist:
        raise Http404
    teams = Team.objects.filter(os=os)
    members = Member.objects.filter(team__os=os)
    return render(request, 'otympligaspelen/os.html', {
        'os': os,
        'teams': teams,
        'members': members,
    })

def team(request, year, team_id):
    try:
        team = Team.objects.get(pk=team_id)
    except Team.DoesNotExist:
        raise Http404
    members = Member.objects.filter(team=team)
    return render(request, 'otympligaspelen/team.html', {
        'year': year,
        'team': team,
        'members': members,
    })

def register(request, year):
    # Check whether registration is open
    try:
        os = Os.objects.get(year=year)
    except Os.DoesNotExist:
        raise Http404
    if not os.registration_open:
        raise Http404
    
    
    
    
    if request.method == 'POST':
        success = False
        full = False

        if register_check(request.POST):
            teams = Team.objects.filter(os=os)
            members = Member.objects.filter(team__os=os)
            
            if members.count() + int(request.POST['num_members']) > os.max_participants:
                full = True
            else:
                register_add(os, request.POST)
                success = True
        
            return HttpResponse(json.dumps({
                'success': success,
                'full': full
            }), content_type="application/json")
        
    else:
        return render(request, 'otympligaspelen/register.html', {
            'year': year,
        })    
    
def games_full(request, year):
    try:
        os = Os.objects.get(year=year)
    except Os.DoesNotExist:
        raise Http404
    
    teams = Team.objects.filter(os=os)
    members = Member.objects.filter(team__os=os)

    return render(request, 'otympligaspelen/games_full.html', {
        'year': os.year, 
        'max': os.max_participants, 
        'current': members.count(),
    })
    
def register_done(request, year):
    return render(request, 'otympligaspelen/register_done.html', {
        'year': year,
    })