# -*- coding: utf-8 -*-
from django.db import models

class Os(models.Model):
    year = models.IntegerField(max_length=4, help_text='År')
    information = models.TextField(help_text='Information: datum, schema, osv')
    registration_open = models.BooleanField(default=False)
    max_participants = models.IntegerField(help_text='Max antal deltagare', default=80)
    
    def __unicode__(self):
        return "OS %d" % self.year

class Team(models.Model):
    name = models.CharField(max_length=128, help_text='Lagnamn')
    email = models.CharField(max_length=128, help_text='Lagets email')
    contact = models.CharField(max_length=64, help_text='Lagets kontaktperson')
    phone = models.CharField(max_length=16, help_text='Telefonnummer')
    motto = models.TextField(help_text='Några väl valda ord')
    
    os = models.ForeignKey(Os)

    def __unicode__(self):
        return "%s" % self.name
    
class Member(models.Model):
    DRINKS = (
        ('O', 'Öhl'),
        ('C', 'Cider'),
        ('L', 'Läsk'),
    )
    SEXES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )

    name = models.CharField(max_length=128, help_text='Namn')
    alias = models.CharField(max_length=128, help_text='Artistnamn')
    function = models.CharField(max_length=128, help_text='Funktion')
    allergies = models.CharField(blank=True, max_length=128, help_text='Allergier')
    drink = models.CharField(max_length=1, choices=DRINKS, help_text='Alkoholpreferns (öhl, cider, läsk)')
    sex = models.CharField(max_length=1, choices=SEXES, help_text='Man/Kvinna')
    team = models.ForeignKey(Team)
    paid = models.IntegerField(default=0, help_text='Betalt')

    def __unicode__(self):
        return "%s" % (self.name)