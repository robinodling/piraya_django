# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from django.views.generic import TemplateView

from apps.otympligaspelen import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^(?P<year>\d+)/$', views.os, name='os'),
    url(r'^(?P<year>\d+)/team/(?P<team_id>\d+)/$', views.team, name='team'),
    url(r'^(?P<year>\d+)/register/$', views.register, name='register'),
    url(r'^(?P<year>\d+)/register/done/$', views.register_done, name='register_done'),
    url(r'^(?P<year>\d+)/register/full/$', views.games_full, name='games_full'),
)
