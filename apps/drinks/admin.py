from django.contrib import admin

from apps.drinks.models import Drink, Ingridient

admin.site.register(Drink)
admin.site.register(Ingridient)
