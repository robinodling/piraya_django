# -*- coding: utf-8 -*-
from django.db import models

class Drink(models.Model):
    name = models.CharField(max_length=64)
    type = models.CharField(max_length=32)
    description = models.TextField()
    sent_by = models.CharField(max_length=64)
    hits = models.IntegerField(default=0)
    votes = models.IntegerField(default=0)
    total_votes = models.IntegerField(default=0)
    approved = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name

class Ingridient(models.Model):
    name = models.CharField(max_length=64)
    quantity = models.CharField(max_length=32)
    drink = models.ForeignKey(Drink)
    
    def __unicode__(self):
        return self.name