# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from django.views.generic import TemplateView

from apps.drinks import views

urlpatterns = patterns('',
        url(r'^$', views.index, name='index'),
        url(r'^send/$', views.send, name='send'),
)
