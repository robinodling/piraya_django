# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect

def index(request):
    return render(request, 'drinks/index.html')

def send(request):
    if request.method == "GET":
        print 'GET'
        return render(request, 'drinks/send.html', {'error': False})
    elif request.method == "POST":
        pass
        try:
            # Get POST data
            num_ingredients = int(request.POST['num_ing'])
            ingredients = []
            for n in range(num_ingredients):
                ingredients.append({
                    'name': request.POST['ing%d' % n],
                    'quantity': request.POST['qua%d' % n],
                })
            
            drink = {
                'name': request.POST['name'],
                'ingredients': ingredients,
                'description': request.POST['description'],
                'type': request.POST['type'],
                'sent_by': request.POST['sentby'],
            }
        except (KeyError, ValueError):
            # Invalid input
            return render(request, 'drinks/send.html', {'error': True})
        else:
            # Try to add drink
            if not Drink.addNewDrink(drink):
                # Invalid input
                return render(request, 'drinks/send.html', {'error': True})
            
            # This prevents data from being posted twice if a
            # user hits the Back button.
            return HttpResponseRedirect()