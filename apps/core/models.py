# -*- coding: utf-8 -*-
from django.db import models

class News(models.Model):
    title = models.CharField(max_length=64)
    content = models.TextField()
    pub_date = models.DateField('Date Published')

    def __unicode__(self):
        return self.title