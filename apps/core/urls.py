# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from django.views.generic import TemplateView

from apps.core import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^contact/$', TemplateView.as_view(template_name="core/contact.html")),
    url(r'^policy/$', TemplateView.as_view(template_name="core/policy.html")),
    url(r'^events/$', TemplateView.as_view(template_name="core/events.html")),
    url(r'^os/$', views.os, name='os'),
)
