from django.contrib import admin

from apps.core.models import News

admin.site.register(News)