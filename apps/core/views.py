# -*- coding: utf-8 -*-
from django.shortcuts import render

from apps.core.models import News

def index(request):
    latest_news_list = News.objects.order_by('-pub_date')[:5]
    return render(request, 'core/index.html', {'latest_news_list': latest_news_list})

def not_found(request):
    return render(request, 'core/404.html')

def os(request):
    return render(request, 'core/os.html')