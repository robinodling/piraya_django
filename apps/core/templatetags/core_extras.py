from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter
@stringfilter
def email_safe(value):
    return value.replace('@', '<ppp>')