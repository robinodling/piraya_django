from django.shortcuts import render

from apps.members.models import Member

def members(request):
    active_list = list(Member.objects.filter(member_status=Member.ACTIVE))
    active_list.sort() # Needs to be sorted
    rookie_list = list(Member.objects.filter(member_status=Member.ROOKIE))
    
    return render(request, 'members/members.html', {
        'active_list': active_list,
        'rookie_list': rookie_list,
    })

def emeritus(request):
    emeritus_list = Member.objects.filter(member_status=Member.EMERITUS).order_by('-year_started')
    return render(request, 'members/emeritus.html', {'emeritus_list': emeritus_list})