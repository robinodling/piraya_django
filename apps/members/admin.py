from django.contrib import admin
from apps.members.models import Member, Titles

admin.site.register(Member)
admin.site.register(Titles)