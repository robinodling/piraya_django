# -*- coding: utf-8 -*-
from django.db import models

class Member(models.Model):
    ROOKIE = 'RO'
    ACTIVE = 'AC'
    EMERITUS = 'EM'
    MEMBER_STATUSES = (
        (ROOKIE, 'Rookie'),
        (ACTIVE, 'Active'),
        (EMERITUS, 'Emeritus'),
    )
    
    #### Personal info
    name = models.CharField(max_length=128, help_text='Full name')
    year_started = models.IntegerField(help_text='Year when started studying (YYYY)')
    # Last known email (preferable @cs.umu.se)
    email = models.CharField(max_length=128, blank=True, help_text='Last known email - or empty')
    # Last known phone number
    phone_number = models.CharField(max_length=16, blank=True, help_text='Last known phone number (not visible for emeritus) - or empty')
    # Nick name (gurka, benzo, etc)
    nick_name = models.CharField(max_length=32, blank=True, help_text='Nick name (gurka, benzo, etc) - or empty')

    #### Piraya info
    member_status = models.CharField(
        max_length=2,
        choices=MEMBER_STATUSES,
        default=ROOKIE,
        help_text='Rookie and Active are only listed in /members/ - Emeritus is only listed in /emeritus/'
    )
    
    #### Site info
    # Funny quote or empty
    quote = models.CharField(max_length=256, blank=True, help_text='Quote - or empty')
    # Picture file name (just no directories)
    picture_file = models.CharField(
        max_length=128,
        help_text='Picture file name (no directories) - You need to (as of now) manually upload a picture to /static/images/members/'
    )

    def __unicode__(self):
        return self.name
        
    def __cmp__(self, other):
        # Get both titles
        my_title = self.get_title()
        other_title = other.get_title()
        
        if my_title == other_title:
            # Same title: equal order
            return 0
        elif my_title == 'Rookie':
            # Rookie > x
            return 1
        elif my_title == 'Piraya' and other_title == 'Rookie':
            # Piraya < Rookie
            return -1
        elif my_title == 'Piraya': # and other_title != 'Rookie'
            # Piraya > x , x != 'Rookie'
            return 1
        
        # Get number of X's
        my_num_x = len([x for x in my_title if x == 'X'])
        other_num_x = len([x for x in other_title if x == 'X'])
        
        if my_num_x < other_num_x:
            return -1
        elif my_num_x > other_num_x:
            return 1
        elif my_title[my_num_x:] == 'SP':
            # SP < SID
            return -1
        else:
            # SID > SP
            return 1

    def get_title(self):
        # First check if Rookie
        if self.member_status == self.ROOKIE:
            return 'Rookie'
        
        # Then check (and take first found) SP/SID title with
        # correct no X's
        titles = Titles.objects.order_by('-year')
        num_x = 0
        for title in titles:
            if title.sp == self:
                return 'X' * num_x + 'SP'
            if title.sid == self:
                return 'X' * num_x + 'SID'
            num_x += 1
        
        # None found => 'Piraya'
        return 'Piraya'
    
    def years_sp(self):
        titles = Titles.objects.filter(sp=self)
        return ", ".join([str(title) for title in titles])
    
    def years_sid(self):
        titles = Titles.objects.filter(sid=self)
        return ", ".join([str(title) for title in titles])

class Titles(models.Model):
    year = models.IntegerField(primary_key=True, help_text='The year that they became SP/SID')
    sp = models.ForeignKey(Member, related_name='title_sp')
    sid = models.ForeignKey(Member, related_name='title_sid')
    
    def __unicode__(self):
        return str(self.year)