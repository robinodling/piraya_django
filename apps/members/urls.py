# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from django.views.generic import TemplateView

from apps.members import views

urlpatterns = patterns('',
        url(r'^$', views.members, name='members'),
        url(r'^emeritus/$', views.emeritus, name='emeritus'),
)
